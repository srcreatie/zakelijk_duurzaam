function animate(callback) {

    setAnimation();

    function setAnimation() {
        __("creative").style.opacity = 1;

        var tl01 = new TimelineMax();
        var tlRollover = new TimelineMax({});

        tl01.from(__("h1"), 1, { x:-100, transformOrigin:"center left", scaleX:0, ease:Back.easeOut });
        tl01.to(__("h1"), 1, { color:"rgba(255,255,255,1)" }, "firstTextIn");
        tl01.to(__("line"), 1, { alpha:1 }, "firstTextIn");

        tl01.to(this, 1.3, {});

        tl01.to(__("h1"), 0.3, { color:"rgba(255,255,255,0)" }, "firstTextOut");
        tl01.to(__("line"), 0.3, { alpha:0 }, "firstTextOut");
        tl01.to(__("h1"), 0.6, { scaleX:0, transformOrigin:"center left" });

        tl01.to(__("bg_2"), 1, { alpha:1 }, "second");

        tl01.from(__("h2"), 1, { x:-100, transformOrigin:"center left", scaleX:0, ease:Back.easeOut }, "second");
        tl01.to(__("h2"), 1, { color:"rgba(255,255,255,1)" });
        
        tl01.to(this, 1.3, {});

        tl01.to(__("h2"), 0.3, { color:"rgba(255,255,255,0)" });
        tl01.to(__("h2"), 0.6, { scaleX:0, transformOrigin:"center left" });

        tl01.to(this, 0.2, {});

        tl01.from(__("pink"), 0.5, { alpha:0 })
        tl01.to(__("stripe_1"), 1, { height:400, transformOrigin:"top center", ease:Back.easeOut })
        tl01.to(__("stripe_2"), 1, { height:400, transformOrigin:"top center", ease:Back.easeOut }, "-=0.5")
        tl01.from(__("end_h1"), 0.5, { alpha:0 }, "end_text-=0.5")
        tl01.from(__("end_txt"), 0.5, { alpha:0 }, "end_text-=0.5")
        
        tl01.fromTo(__("icon"), 1, { alpha:0, scale:0, rotation:-15, ease:Back.easeIn }, { alpha:1, scale:1.5, rotation:15  }, "end_text-=0.5")
        tl01.to(__("icon"), 0.75, { scale:1, rotation:0, ease:Back.easeOut  }, "end_text+=0.75")
        
        tl01.to(__("cta"), 0.5, { scale:1.1, yoyo:true, repeat:1, ease:Power2.easeInOut, transformOrigin:"center center" });
        tl01.to(this, 0, { onComplete:rollover });

        tlRollover.to(__("cta"), 0.5, { scale:1.1, yoyo:true, repeat:1, ease:Power2.easeInOut, transformOrigin:"center center" }, "rollover");

        function rollover(){
            __("banner").onmouseover = function(){
                tlRollover.play(0);
            };

            __("banner").onmouseout = function(){
                tlRollover.reverse();
            };
        }

        if (srBanner.debug) {
            if (srBanner.debug && srBanner.pauseFrom) {
                console.log("pause from " + srBanner.pauseFrom);
                tl01.pause(srBanner.pauseFrom);
            }

            if (srBanner.debug && srBanner.playFrom) {
                console.log("play from " + srBanner.playFrom);
                tl01.pause(srBanner.playFrom);
            }

            if (srBanner && srBanner.backupImage) {
                console.log("create backup images of last frame");
                tl01.add("screenshot");
                tl01.pause("screenshot");
            }
        }
    }

    if (callback) {
        callback();
    }
}

module.exports.animate = animate;