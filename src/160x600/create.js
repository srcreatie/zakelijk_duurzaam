function setElements(callback) {

    config = {};
    config.bannerWidth = 160;
    config.bannerHeight = 600;

    addClicktag(dd.exit_url);
    loadFont();

    function loadFont() {
        sr.font.add([asset('bold.woff'),
            asset('semibold.woff')
        ], add);
    }

    function add() {
            
        ___("bg_1")
            .image(dd.background160x600_1, { width:160, height:600, fit:true });

        ___("bg_2")
            .image(dd.background160x600_2, { width:160, height:600, fit:true })
            .style({ greensock:{ alpha:0 } });

        ___("h1")
            .text(dd.copy.h1, { fontSize:17, color:"rgba(255,255,255,0)", webfont:"semibold" })
            .style({ background:"#D92266", css:"max-width:140px;" })
            .position({ left:10, top:260 });

        ___("h2")
            .text(dd.copy.h2, { fontSize:17, color:"rgba(255,255,255,0)", webfont:"semibold" })
            .style({ background:"#D92266", css:"max-width:140px;" })
            .position({ centerX:0, top:260 });

        ___("pink")
            .style({ background:"#e60167", width:config.bannerWidth, height:config.bannerHeight })

        ___("stripe_1")
            .style({ background:"rgba(0,0,0,0.21)", width:150, height:0, greensock:{ rotation:-75 } })
            .position({ top:425, left:-180 })

        ___("stripe_2")
            .style({ background:"rgba(0,0,0,0.21)", width:150, height:0, greensock:{ rotation:50 } })
            .position({ top:380, left:150 })

        ___("end_h1")
            .text(dd.copy.end_h1, { fontSize:22, color:"#fff", webfont:"bold" })
            .style({ css:"max-width:140px;" })
            .position({ left:10, top:44 })

        ___("end_txt")
            .text(dd.copy.end_txt, { fontSize:16, color:"#fff", webfont:"semibold" })
            .style({ css:"max-width:140px;" })
            .position({ left:10, push:{ el:__("end_h1"), bottom:6 } })

        if (dd.icon != "") {
            ___("icon")
                .image(asset("check_icon.png"), { width:81, height:75, fit:true })
                .position({ left:10, push: {el:__("end_txt"), bottom:15 }})
        }

        ___("footer")
            .style({ width:160, height:135, background:"#fff" })
            .position({ bottom:0, left:0 });

        ___("footer>cta")
            .text(dd.cta, { webfont:"bold", fontSize:15, color:"#fff", textAlign:"center" })
            .style({ background:"#02B5CE", css:"border-radius:3px; padding:7px 11px; border-bottom:2px solid #016E81;" })
            .position({ top:32, centerX:0 });

        ___("footer>logo")
            .image(asset("logo_pink.png"), { width:90, height:30, fit:true })
            .position({ centerX:0, top:90 });

        sr.loading.done(callback);
    }
}


function addClicktag(customUrl) {
    document.getElementById("bg-exit").addEventListener('click', bgExitHandler, false);

    function bgExitHandler(e) {
        if (srBanner.dcs) {
            Enabler.exit('Background Exit');
        } else {
            window.open(clickTag);
        }
    }
}

module.exports.setElements = setElements;