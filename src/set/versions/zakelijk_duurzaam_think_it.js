function setData(callback) {

    var devDynamicContent = {};

    devDynamicContent.srFeed = [{}];
    devDynamicContent.srFeed[0]._id = 0;
    devDynamicContent.srFeed[0].isWorking = "Default feed 2";
    devDynamicContent.srFeed[0].exit_url = "";
    devDynamicContent.srFeed[0].versie = "think_it";
    devDynamicContent.srFeed[0].copy = {};
    devDynamicContent.srFeed[0].copy.h1 = "(30 fte + 650m2) x IT bedrijf<br /><span id=\"line\"></span>=<br />Wet Milieubeheer"; 
    devDynamicContent.srFeed[0].copy.h2 = "Ingewikkeld?";
    devDynamicContent.srFeed[0].copy.end_h1 = "Energie besparen wordt steeds makkelijker";
    devDynamicContent.srFeed[0].copy.end_txt = "Check in 1 minuut aan welke verplichtingen uw bedrijf moet voldoen";

    devDynamicContent.srFeed[0].cta = "Doe de check";
    devDynamicContent.srFeed[0].cta_sky = "Doe de check";
    
    devDynamicContent.srFeed[0].icon = asset("check_icon.png");

    devDynamicContent.srFeed[0].background336x280_1 = dimension("it-1-336x280.jpg", "336x280");
    devDynamicContent.srFeed[0].background336x280_2 = dimension("it-1-336x280.jpg", "336x280");

    devDynamicContent.srFeed[0].background300x250_1 = dimension("it-1-300x250.jpg", "300x250");
    devDynamicContent.srFeed[0].background300x250_2 = dimension("it-1-300x250.jpg", "300x250");

    devDynamicContent.srFeed[0].background320x240_1 = dimension("it-1-300x250.jpg", "320x240");
    devDynamicContent.srFeed[0].background320x240_2 = dimension("it-1-300x250.jpg", "320x240");

    devDynamicContent.srFeed[0].background728x90_1 = dimension("it-1-728x90.jpg", "728x90");
    devDynamicContent.srFeed[0].background728x90_2 = dimension("it-1-728x90.jpg", "728x90");

    devDynamicContent.srFeed[0].background970x250_1 = dimension("it-1-970x250.jpg", "970x250");
    devDynamicContent.srFeed[0].background970x250_2 = dimension("it-1-970x250.jpg", "970x250");

    devDynamicContent.srFeed[0].background120x600_1 = dimension("it-1-120x600.jpg", "120x600");
    devDynamicContent.srFeed[0].background120x600_2 = dimension("it-1-120x600.jpg", "120x600");

    devDynamicContent.srFeed[0].background160x600_1 = dimension("it-1-160x600.jpg", "160x600");
    devDynamicContent.srFeed[0].background160x600_2 = dimension("it-1-160x600.jpg", "160x600");

    devDynamicContent.srFeed[0].background300x600_1 = dimension("it-1-300x600.jpg", "300x600");
    devDynamicContent.srFeed[0].background300x600_2 = dimension("it-1-300x600.jpg", "300x600");
    return devDynamicContent;
}

module.exports = setData;