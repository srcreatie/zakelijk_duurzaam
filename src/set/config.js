function loadSettings(callback) {

    var manifest = require('./../set/manifest.json');


    srBanner = {};
    srBanner.useDcs = manifest.data.dcs;
    srBanner.debug = manifest.debugging.debug;
    srBanner.playFrom = manifest.debugging.playFrom;
    srBanner.pauseFrom = manifest.debugging.pauseFrom;
    srBanner.backupImage = manifest.debugging.backupImage;

    //srbannerIslocal
    srBanner.isLocal = true;
    //endsrbannerIslocal
    srBanner.localFolder = "../set/assets/";

    srBanner.dynamicData = require('feedType');

    srBanner.feed = manifest.data.feed;

    if (srBanner.debug) {

        console.log("%c DEBUGGING SET TO TRUE", 'background: #FF0000; color: #FFFFFF');
    }

    asset = function(assetToLoad) {

        if (srBanner && srBanner.isLocal) {

            if (srBanner.debug) {
                console.log("%cload asset >>>>>> " + assetToLoad, 'background: #00c1ff; color: #FFFFFF');
            }
            return srBanner.localFolder + assetToLoad;

        }

        return "./" + assetToLoad;
    }

    dimension = function(assetToLoad, formatString) {


        console.log("asset to load from dimension");
        console.log(assetToLoad);

        console.log("srbanner is local: " + srBanner.isLocal)

        if (srBanner && srBanner.isLocal) {

            if (srBanner.debug) {
                console.log("%cload asset >>>>>> " + assetToLoad, 'background: #00c1ff; color: #FFFFFF');
            }
            return srBanner.localFolder + assetToLoad;

        }

  

        return "./" + assetToLoad;
    }


    return srBanner;

}


module.exports.loadSettings = loadSettings;