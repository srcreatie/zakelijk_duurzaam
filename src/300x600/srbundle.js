/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "assets";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(1);
var setConfig = __webpack_require__(2);
var create = __webpack_require__(6);
var interact = __webpack_require__(7);

var srBanner = setConfig.loadSettings();
var dynamicData = srBanner.dynamicData;



if (srBanner.debug) {
    console.log('%c ==============SR Banner props=============', 'background: #0060a1; color: #FFFFFF');
    // console.log('%c Oh my heavens! ', 'background: #222; color: #bada55');
    console.table(srBanner, ["Value"]);
    console.log('%c __________________________________________', 'background: #0060a1; color: #FFFFFF');
}

window.onload = setupBanner;

function setupBanner() {

    //load data 
    dynamicData.setup(function(dynamicData) {

        //data loaded, create global var dd and fill it with data 
        dd = dynamicData;

        //create and set elements
        create.setElements(function() {

            //after set start setup of animation
            interact.animate();

        });
    });
}

/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

var require;var require;!function(a){if(true)module.exports=a();else if("function"==typeof define&&define.amd)define([],a);else{var b;b="undefined"!=typeof window?window:"undefined"!=typeof global?global:"undefined"!=typeof self?self:this,b.sr=a()}}(function(){var a;return function a(b,c,d){function e(g,h){if(!c[g]){if(!b[g]){var i="function"==typeof require&&require;if(!h&&i)return require(g,!0);if(f)return f(g,!0);var j=new Error("Cannot find module '"+g+"'");throw j.code="MODULE_NOT_FOUND",j}var k=c[g]={exports:{}};b[g][0].call(k.exports,function(a){var c=b[g][1][a];return e(c||a)},k,k.exports,a,b,c,d)}return c[g].exports}for(var f="function"==typeof require&&require,g=0;g<d.length;g++)e(d[g]);return e}({1:[function(a,b,c){},{}],2:[function(a,b,c){function d(a,b){l=b,e(a)}function e(a){for(var b=h(a),c="",d=0;d<b.length;d++)c+="@font-face { font-family:"+b[d].name+"; src: url("+b[d].filename+"); }    ",j.push(b[d].name);var e=document.createElement("style");e.type="text/css",e.textContent=c,document.head.appendChild(e),f()}function f(){i.load({custom:{families:j},fontloading:function(){},fontactive:function(){g()},fontinactive:function(a,b){console.log("failed to load"+a),g()}})}function g(){++k==j.length&&l()}function h(a){for(var b=[],c=0;c<a.length;c++)if("woff"==a[c].substring(a[c].length-4,a[c].length)){var d=a[c].split("/");d=d[d.length-1],b.push({name:d.substring(0,d.length-5),filename:a[c]})}else console.log("only woff fonts are supported for now");return b}var i=a("./lib/webfontloader.js"),j=[],k=0,l=function(){};b.exports.add=d},{"./lib/webfontloader.js":6}],3:[function(a,b,c){sr=a("./srmain.js"),srElements=[],srPixels={},_underscoresr=a("./underscoresr.js"),___=function(a){if(Array.isArray(a)){var b=new _underscoresr;return b.elementArray=b.getArray(a),b}var b=new _underscoresr;return b.element=a,b.self=b,b.get(a),b},__=function(a){if(Array.isArray(a)){for(var b=[],c=0;c<a.length;c++)b.push(___(a[c]).element);return b}return ___(a).element}},{"./srmain.js":8,"./underscoresr.js":11}],4:[function(a,b,c){function d(a){if(Array.isArray(a)){for(var b=[],c=0;c<a.length;c++)a[c].tagName,b.push(a[c]);for(var d=e(b),c=0;c<b.length;c++)b[c].style.fontSize=String(d)+"px"}else console.log("input needs to be array")}function e(a){for(var b=99999,c=0;c<a.length;c++){var d=a[c].style.fontSize,e=parseFloat(d.substring(0,d.length-2));e<=b&&(b=e)}return b}function f(a){return{left:k(a,"left"),right:l(a,"left","offsetWidth"),top:k(a,"top"),bottom:l(a,"top","offsetHeight")}}function g(a){var b=k(a,"top");return l(a,"top","offsetHeight")-b}function h(a){var b=k(a,"left");return l(a,"left","offsetWidth")-b}function i(a,b){if(b.el||(b.el=__("creative")),b.left||0==b.left){var c=k(a,"left"),d=l(a,"left","offsetWidth"),e=d-c,f=b.el.offsetWidth,g=b.el.style.left||"0px",h=parseFloat(g.substring(0,g.length-2)),i=h,m=i-c+b.left;j(a,m,"left")}if(b.right||0==b.right){var c=k(a,"left"),d=l(a,"left","offsetWidth"),e=d-c,f=b.el.offsetWidth,g=b.el.style.left||"0px",h=parseFloat(g.substring(0,g.length-2)),i=f-e+h,m=i-c-b.right;j(a,m,"left")}if(b.top||0==b.top){var n=k(a,"top"),o=l(a,"top","offsetHeight"),p=o-n,m=b.top;j(a,m,"top")}if(b.bottom||0==b.bottom){var n=k(a,"top"),o=l(a,"top","offsetHeight"),p=o-n,q=b.el.offsetHeight,r=b.el.style.top||"0px",s=parseFloat(r.substring(0,r.length-2)),i=q-p+s,m=i-n-b.bottom;j(a,m,"top")}if(b.centerX||0==b.centerX){var c=k(a,"left"),d=l(a,"left","offsetWidth"),e=d-c,f=b.el.offsetWidth,g=b.el.style.left||"0px",h=parseFloat(g.substring(0,g.length-2)),i=f/2-e/2+h,m=i-c;j(a,m,"left")}if(b.centerY||0==b.centerY){var n=k(a,"top"),o=l(a,"top","offsetHeight"),p=o-n,q=b.el.offsetHeight,r=b.el.style.top||"0px",s=parseFloat(r.substring(0,r.length-2)),i=q/2-p/2+s,m=i-n;j(a,m,"top")}}function j(a,b,c){for(var d=0;d<a.length;d++){var e=a[d].style[c]||"0px";newValue=e.substring(0,e.length-2),a[d].style[c]=parseFloat(newValue,10)+b+"px"}}function k(a,b){for(var c=999999,d=0;d<a.length;d++){var e=a[d].style[b]||"0px";newValue=e.substring(0,e.length-2),newValue<c&&(c=parseFloat(newValue,10))}return c}function l(a,b,c){for(var d=0,e=0;e<a.length;e++){var f=a[e].style[b]||"0px";newValue=parseFloat(f.substring(0,f.length-2),10)+a[e][c],newValue>d&&(d=parseFloat(newValue,10))}return d}b.exports.groupGetPosition=f,b.exports.groupHeight=g,b.exports.groupWidth=h,b.exports.groupFs=d,b.exports.position=i},{}],5:[function(a,b,c){function d(a,b,c,d){function i(a,b){this._dimensions=h(a),this._vars.wrap?f.call(this,a,this._element):this._vars.fit&&e.call(this,a,this._element),this._vars.align&&g.call(this,a),this._element.appendChild(a),this._vars.onComplete&&this._vars.onCompleteParams&&this._vars.onComplete({width:this._element.style.width,height:this._element.style.height,params:this._vars.onCompleteParams}),d(this,"succes")}if(this._element=a||{},this._vars=c,this._input=b||"",!this._element)return void console.log("kan element niet vinden");var j=new Image;j.onerror=function(){d(this,"error")},j.onload=function(){i.call(k,this)},j.src=this._input;var k=this;this._vars.width&&(this._element.style.width=this._vars.width+"px"),this._vars.height&&(this._element.style.height=this._vars.height+"px")}function e(a,b){if(b||"")var c=b.offsetHeight/this._dimensions.height,d=b.offsetWidth/this._dimensions.width;else var c=context.bannerHeight/this._dimensions.height,d=context.bannerWidth/this._dimensions.width;var e=c<d?c:d;a.width=this._dimensions.width*e,a.height=this._dimensions.height*e,this._vars.autoWidth&&(b.style.width=a.width+"px"),this._vars.autoHeight&&(b.style.height=a.height+"px"),this._vars.autoSize&&(b.style.width=a.width+"px",b.style.height=a.height+"px")}function f(a,b){var c=b||"";if(!c)return void console.log("no element privided");var d=b.offsetHeight/this._dimensions.height,e=b.offsetWidth/this._dimensions.width;c.style.overflow="hidden";var f=d>e?d:e;a.width=this._dimensions.width*f,a.height=this._dimensions.height*f}function g(a){a.style.position="absolute";var b=this._vars.align.split(" ")||[];"center"==b[0]&&(a.style.top=(this._element.offsetHeight-a.height)/2+"px"),"bottom"==b[0]&&(a.style.top=this._element.offsetHeight-a.height+"px"),"top"==b[0]&&(a.style.top="0px"),"center"==b[1]&&(a.style.left=(this._element.offsetWidth-a.width)/2+"px"),"right"==b[1]&&(a.style.left=this._element.offsetWidth-a.width+"px"),"left"==b[1]&&(a.style.left="0px")}function h(a){if(a.naturalWidth)return{width:a.naturalWidth,height:a.naturalHeight};var b=new Image;return b.src=a.src,{width:b.width,height:b.height}}b.exports=d},{}],6:[function(b,c,d){!function(){function b(a,b,c){return a.call.apply(a.bind,arguments)}function d(a,b,c){if(!a)throw Error();if(2<arguments.length){var d=Array.prototype.slice.call(arguments,2);return function(){var c=Array.prototype.slice.call(arguments);return Array.prototype.unshift.apply(c,d),a.apply(b,c)}}return function(){return a.apply(b,arguments)}}function e(a,c,f){return e=Function.prototype.bind&&-1!=Function.prototype.bind.toString().indexOf("native code")?b:d,e.apply(null,arguments)}function f(a,b){this.F=a,this.k=b||a,this.H=this.k.document}function g(a,b,c){a=a.H.getElementsByTagName(b)[0],a||(a=document.documentElement),a.insertBefore(c,a.lastChild)}function h(a,b,c){b=b||[],c=c||[];for(var d=a.className.split(/\s+/),e=0;e<b.length;e+=1){for(var f=!1,g=0;g<d.length;g+=1)if(b[e]===d[g]){f=!0;break}f||d.push(b[e])}for(b=[],e=0;e<d.length;e+=1){for(f=!1,g=0;g<c.length;g+=1)if(d[e]===c[g]){f=!0;break}f||b.push(d[e])}a.className=b.join(" ").replace(/\s+/g," ").replace(/^\s+|\s+$/,"")}function i(a,b){for(var c=a.className.split(/\s+/),d=0,e=c.length;d<e;d++)if(c[d]==b)return!0;return!1}function j(a){if("string"==typeof a.fa)return a.fa;var b=a.k.location.protocol;return"about:"==b&&(b=a.F.location.protocol),"https:"==b?"https:":"http:"}function k(a,b,c){function d(){i&&e&&f&&(i(h),i=null)}b=a.createElement("link",{rel:"stylesheet",href:b,media:"all"});var e=!1,f=!0,h=null,i=c||null;_?(b.onload=function(){e=!0,d()},b.onerror=function(){e=!0,h=Error("Stylesheet failed to load"),d()}):setTimeout(function(){e=!0,d()},0),g(a,"head",b)}function l(a,b,c,d){var e=a.H.getElementsByTagName("head")[0];if(e){var f=a.createElement("script",{src:b}),g=!1;return f.onload=f.onreadystatechange=function(){g||this.readyState&&"loaded"!=this.readyState&&"complete"!=this.readyState||(g=!0,c&&c(null),f.onload=f.onreadystatechange=null,"HEAD"==f.parentNode.tagName&&e.removeChild(f))},e.appendChild(f),setTimeout(function(){g||(g=!0,c&&c(Error("Script load timeout")))},d||5e3),f}return null}function m(){this.S=0,this.K=null}function n(a){return a.S++,function(){a.S--,p(a)}}function o(a,b){a.K=b,p(a)}function p(a){0==a.S&&a.K&&(a.K(),a.K=null)}function q(a){this.ea=a||"-"}function r(a,b){this.Q=a,this.M=4,this.L="n";var c=(b||"n4").match(/^([nio])([1-9])$/i);c&&(this.L=c[1],this.M=parseInt(c[2],10))}function s(a){return v(a)+" "+a.M+"00 300px "+t(a.Q)}function t(a){var b=[];a=a.split(/,\s*/);for(var c=0;c<a.length;c++){var d=a[c].replace(/['"]/g,"");-1!=d.indexOf(" ")||/^\d/.test(d)?b.push("'"+d+"'"):b.push(d)}return b.join(",")}function u(a){return a.L+a.M}function v(a){var b="normal";return"o"===a.L?b="oblique":"i"===a.L&&(b="italic"),b}function w(a){var b=4,c="n",d=null;return a&&((d=a.match(/(normal|oblique|italic)/i))&&d[1]&&(c=d[1].substr(0,1).toLowerCase()),(d=a.match(/([1-9]00|normal|bold)/i))&&d[1]&&(/bold/i.test(d[1])?b=7:/[1-9]00/.test(d[1])&&(b=parseInt(d[1].substr(0,1),10)))),c+b}function x(a,b){this.a=a,this.j=a.k.document.documentElement,this.O=b,this.f="wf",this.e=new q("-"),this.da=!1!==b.events,this.u=!1!==b.classes}function y(a){a.u&&h(a.j,[a.e.d(a.f,"loading")]),A(a,"loading")}function z(a){if(a.u){var b=i(a.j,a.e.d(a.f,"active")),c=[],d=[a.e.d(a.f,"loading")];b||c.push(a.e.d(a.f,"inactive")),h(a.j,c,d)}A(a,"inactive")}function A(a,b,c){a.da&&a.O[b]&&(c?a.O[b](c.getName(),u(c)):a.O[b]())}function B(){this.t={}}function C(a,b,c){var d,e=[];for(d in b)if(b.hasOwnProperty(d)){var f=a.t[d];f&&e.push(f(b[d],c))}return e}function D(a,b){this.a=a,this.h=b,this.m=this.a.createElement("span",{"aria-hidden":"true"},this.h)}function E(a,b){var c,d=a.m;c="display:block;position:absolute;top:-9999px;left:-9999px;font-size:300px;width:auto;height:auto;line-height:normal;margin:0;padding:0;font-variant:normal;white-space:nowrap;font-family:"+t(b.Q)+";font-style:"+v(b)+";font-weight:"+b.M+"00;",d.style.cssText=c}function F(a){g(a.a,"body",a.m)}function G(a,b,c,d,e,f){this.G=a,this.J=b,this.g=d,this.a=c,this.v=e||3e3,this.h=f||void 0}function H(a,b,c,d,e,f,g){this.G=a,this.J=b,this.a=c,this.g=d,this.h=g||"BESbswy",this.s={},this.v=e||3e3,this.Z=f||null,this.D=this.C=this.A=this.w=null,this.w=new D(this.a,this.h),this.A=new D(this.a,this.h),this.C=new D(this.a,this.h),this.D=new D(this.a,this.h),E(this.w,new r(this.g.getName()+",serif",u(this.g))),E(this.A,new r(this.g.getName()+",sans-serif",u(this.g))),E(this.C,new r("serif",u(this.g))),E(this.D,new r("sans-serif",u(this.g))),F(this.w),F(this.A),F(this.C),F(this.D)}function I(){if(null===ba){var a=/AppleWebKit\/([0-9]+)(?:\.([0-9]+))/.exec(window.navigator.userAgent);ba=!!a&&(536>parseInt(a[1],10)||536===parseInt(a[1],10)&&11>=parseInt(a[2],10))}return ba}function J(a,b,c){for(var d in aa)if(aa.hasOwnProperty(d)&&b===a.s[aa[d]]&&c===a.s[aa[d]])return!0;return!1}function K(a){var b,c=a.w.m.offsetWidth,d=a.A.m.offsetWidth;(b=c===a.s.serif&&d===a.s["sans-serif"])||(b=I()&&J(a,c,d)),b?$()-a.ga>=a.v?I()&&J(a,c,d)&&(null===a.Z||a.Z.hasOwnProperty(a.g.getName()))?M(a,a.G):M(a,a.J):L(a):M(a,a.G)}function L(a){setTimeout(e(function(){K(this)},a),50)}function M(a,b){setTimeout(e(function(){this.w.remove(),this.A.remove(),this.C.remove(),this.D.remove(),b(this.g)},a),0)}function N(a,b,c){this.a=a,this.p=b,this.P=0,this.ba=this.Y=!1,this.v=c}function O(a){0==--a.P&&a.Y&&(a.ba?(a=a.p,a.u&&h(a.j,[a.e.d(a.f,"active")],[a.e.d(a.f,"loading"),a.e.d(a.f,"inactive")]),A(a,"active")):z(a.p))}function P(a){this.F=a,this.q=new B,this.$=0,this.T=this.U=!0}function Q(a,b,c,d,f){var g=0==--a.$;(a.T||a.U)&&setTimeout(function(){var a=f||null,i=d||null||{};if(0===c.length&&g)z(b.p);else{b.P+=c.length,g&&(b.Y=g);var j,k=[];for(j=0;j<c.length;j++){var l=c[j],m=i[l.getName()],n=b.p,o=l;n.u&&h(n.j,[n.e.d(n.f,o.getName(),u(o).toString(),"loading")]),A(n,"fontloading",o),n=null,null===ca&&(ca=!!window.FontFace&&(!(n=/Gecko.*Firefox\/(\d+)/.exec(window.navigator.userAgent))||42<parseInt(n[1],10))),n=ca?new G(e(b.V,b),e(b.W,b),b.a,l,b.v,m):new H(e(b.V,b),e(b.W,b),b.a,l,b.v,a,m),k.push(n)}for(j=0;j<k.length;j++)k[j].start()}},0)}function R(a,b,c){var d=[],e=c.timeout;y(b);var d=C(a.q,c,a.a),f=new N(a.a,b,e);for(a.$=d.length,b=0,c=d.length;b<c;b++)d[b].load(function(b,c,d){Q(a,f,b,c,d)})}function S(a,b,c){this.N=a||b+da,this.o=[],this.R=[],this.ca=c||""}function T(a,b){for(var c=b.length,d=0;d<c;d++){var e=b[d].split(":");3==e.length&&a.R.push(e.pop());var f="";2==e.length&&""!=e[1]&&(f=":"),a.o.push(e.join(f))}}function U(a){this.o=a,this.aa=[],this.I={}}function V(a,b){this.a=a,this.c=b}function W(a,b){this.a=a,this.c=b,this.X=[]}function X(a,b){this.a=a,this.c=b}function Y(a,b){this.a=a,this.c=b}function Z(a,b){this.a=a,this.c=b}var $=Date.now||function(){return+new Date},_=!!window.FontFace;f.prototype.createElement=function(a,b,c){if(a=this.H.createElement(a),b)for(var d in b)b.hasOwnProperty(d)&&("style"==d?a.style.cssText=b[d]:a.setAttribute(d,b[d]));return c&&a.appendChild(this.H.createTextNode(c)),a},q.prototype.d=function(a){for(var b=[],c=0;c<arguments.length;c++)b.push(arguments[c].replace(/[\W_]+/g,"").toLowerCase());return b.join(this.ea)},r.prototype.getName=function(){return this.Q},D.prototype.remove=function(){var a=this.m;a.parentNode&&a.parentNode.removeChild(a)},G.prototype.start=function(){function a(){$()-d>=c.v?c.J(c.g):b.fonts.load(s(c.g),c.h).then(function(b){1<=b.length?c.G(c.g):setTimeout(a,25)},function(){c.J(c.g)})}var b=this.a.k.document,c=this,d=$();a()};var aa={ia:"serif",ha:"sans-serif"},ba=null;H.prototype.start=function(){this.s.serif=this.C.m.offsetWidth,this.s["sans-serif"]=this.D.m.offsetWidth,this.ga=$(),K(this)};var ca=null;N.prototype.V=function(a){var b=this.p;b.u&&h(b.j,[b.e.d(b.f,a.getName(),u(a).toString(),"active")],[b.e.d(b.f,a.getName(),u(a).toString(),"loading"),b.e.d(b.f,a.getName(),u(a).toString(),"inactive")]),A(b,"fontactive",a),this.ba=!0,O(this)},N.prototype.W=function(a){var b=this.p;if(b.u){var c=i(b.j,b.e.d(b.f,a.getName(),u(a).toString(),"active")),d=[],e=[b.e.d(b.f,a.getName(),u(a).toString(),"loading")];c||d.push(b.e.d(b.f,a.getName(),u(a).toString(),"inactive")),h(b.j,d,e)}A(b,"fontinactive",a),O(this)},P.prototype.load=function(a){this.a=new f(this.F,a.context||this.F),this.U=!1!==a.events,this.T=!1!==a.classes,R(this,new x(this.a,a),a)};var da="//fonts.googleapis.com/css";S.prototype.d=function(){if(0==this.o.length)throw Error("No fonts to load!");if(-1!=this.N.indexOf("kit="))return this.N;for(var a=this.o.length,b=[],c=0;c<a;c++)b.push(this.o[c].replace(/ /g,"+"));return a=this.N+"?family="+b.join("%7C"),0<this.R.length&&(a+="&subset="+this.R.join(",")),0<this.ca.length&&(a+="&text="+encodeURIComponent(this.ca)),a};var ea={latin:"BESbswy",cyrillic:"&#1081;&#1103;&#1046;",greek:"&#945;&#946;&#931;",khmer:"&#x1780;&#x1781;&#x1782;",Hanuman:"&#x1780;&#x1781;&#x1782;"},fa={thin:"1",extralight:"2","extra-light":"2",ultralight:"2","ultra-light":"2",light:"3",regular:"4",book:"4",medium:"5","semi-bold":"6",semibold:"6","demi-bold":"6",demibold:"6",bold:"7","extra-bold":"8",extrabold:"8","ultra-bold":"8",ultrabold:"8",black:"9",heavy:"9",l:"3",r:"4",b:"7"},ga={i:"i",italic:"i",n:"n",normal:"n"},ha=/^(thin|(?:(?:extra|ultra)-?)?light|regular|book|medium|(?:(?:semi|demi|extra|ultra)-?)?bold|black|heavy|l|r|b|[1-9]00)?(n|i|normal|italic)?$/;U.prototype.parse=function(){for(var a=this.o.length,b=0;b<a;b++){var c=this.o[b].split(":"),d=c[0].replace(/\+/g," "),e=["n4"];if(2<=c.length){var f,g=c[1];if(f=[],g)for(var g=g.split(","),h=g.length,i=0;i<h;i++){var j;if(j=g[i],j.match(/^[\w-]+$/))if(null==(j=ha.exec(j.toLowerCase())))j="";else{var k;if(null==(k=j[1])||""==k)k="4";else{var l=fa[k];k=l||(isNaN(k)?"4":k.substr(0,1))}j=j[2],j=[null==j||""==j?"n":ga[j],k].join("")}else j="";j&&f.push(j)}0<f.length&&(e=f),3==c.length&&(c=c[2],f=[],c=c?c.split(","):f,0<c.length&&(c=ea[c[0]])&&(this.I[d]=c))}for(this.I[d]||(c=ea[d])&&(this.I[d]=c),c=0;c<e.length;c+=1)this.aa.push(new r(d,e[c]))}};var ia={Arimo:!0,Cousine:!0,Tinos:!0};V.prototype.load=function(a){var b=new m,c=this.a,d=new S(this.c.api,j(c),this.c.text),e=this.c.families;T(d,e);var f=new U(e);f.parse(),k(c,d.d(),n(b)),o(b,function(){a(f.aa,f.I,ia)})},W.prototype.B=function(a){var b=this.a;return j(this.a)+(this.c.api||"//f.fontdeck.com/s/css/js/")+(b.k.location.hostname||b.F.location.hostname)+"/"+a+".js"},W.prototype.load=function(a){var b=this.c.id,c=this.a.k,d=this;b?(c.__webfontfontdeckmodule__||(c.__webfontfontdeckmodule__={}),c.__webfontfontdeckmodule__[b]=function(b,c){for(var e=0,f=c.fonts.length;e<f;++e){var g=c.fonts[e];d.X.push(new r(g.name,w("font-weight:"+g.weight+";font-style:"+g.style)))}a(d.X)},l(this.a,this.B(b),function(b){b&&a([])})):a([])},X.prototype.B=function(a){return(this.c.api||"https://use.typekit.net")+"/"+a+".js"},X.prototype.load=function(a){var b=this.c.id,c=this.a.k;b?l(this.a,this.B(b),function(b){if(b)a([]);else if(c.Typekit&&c.Typekit.config&&c.Typekit.config.fn){b=c.Typekit.config.fn;for(var d=[],e=0;e<b.length;e+=2)for(var f=b[e],g=b[e+1],h=0;h<g.length;h++)d.push(new r(f,g[h]));try{c.Typekit.load({events:!1,classes:!1,async:!0})}catch(a){}a(d)}},2e3):a([])},Y.prototype.B=function(a,b){return j(this.a)+"//"+(this.c.api||"fast.fonts.net/jsapi").replace(/^.*http(s?):(\/\/)?/,"")+"/"+a+".js"+(b?"?v="+b:"")},Y.prototype.load=function(a){function b(){if(e["__mti_fntLst"+c]){var d,f=e["__mti_fntLst"+c](),g=[];if(f)for(var h=0;h<f.length;h++){var i=f[h].fontfamily;void 0!=f[h].fontStyle&&void 0!=f[h].fontWeight?(d=f[h].fontStyle+f[h].fontWeight,g.push(new r(i,d))):g.push(new r(i))}a(g)}else setTimeout(function(){b()},50)}var c=this.c.projectId,d=this.c.version;if(c){var e=this.a.k;l(this.a,this.B(c,d),function(c){c?a([]):b()}).id="__MonotypeAPIScript__"+c}else a([])},Z.prototype.load=function(a){var b,c,d=this.c.urls||[],e=this.c.families||[],f=this.c.testStrings||{},g=new m;for(b=0,c=d.length;b<c;b++)k(this.a,d[b],n(g));var h=[];for(b=0,c=e.length;b<c;b++)if(d=e[b].split(":"),d[1])for(var i=d[1].split(","),j=0;j<i.length;j+=1)h.push(new r(d[0],i[j]));else h.push(new r(d[0]));o(g,function(){a(h,f)})};var ja=new P(window);ja.q.t.custom=function(a,b){return new Z(b,a)},ja.q.t.fontdeck=function(a,b){return new W(b,a)},ja.q.t.monotype=function(a,b){return new Y(b,a)},ja.q.t.typekit=function(a,b){return new X(b,a)},ja.q.t.google=function(a,b){return new V(b,a)};var ka={load:e(ja.load,ja)};"function"==typeof a&&a.amd?a(function(){return ka}):void 0!==c&&c.exports?c.exports=ka:(window.WebFont=ka,window.WebFontConfig&&ja.load(window.WebFontConfig))}()},{}],7:[function(a,b,c){function d(a,b,c){if(this._element=a,this._vars=b||{},this._self=c||{},!this._element)return void console.log("kan element niet vinden");this._element.style.position="absolute",this._vars.addClass&&(a.className+=" "+this._vars.addClass),this._vars.css&&(cssProps=b.css,a.style.cssText+=";"+cssProps),isNaN(this._vars.width)||(a.style.width=this._vars.width+"px"),isNaN(this._vars.height)||(a.style.height=this._vars.height+"px"),e.call(this),this._vars.push&&f.call(this),this._vars.between&&positionBetween.call(this)}function e(){if("object"==typeof this._vars&&null!==this._vars){this._element.style.display="inline-block",this._element.style.clear="both",this._element.style.position="absolute";var a=0,b=this._element.getBoundingClientRect().width;if(0===b&&(b=this._element.scrollWidth),!this._vars.el){var c=document.getElementById("creative"),d=document.getElementById("banner");if(c)this._vars.el=c;else{if(!d)return void console.log("creative div not found");this._vars.el=d}}var e=this._element.getBoundingClientRect().height,f=this._vars.el.getBoundingClientRect();if(this._vars.left||0==this._vars.left||this._vars.right||0==this._vars.right||this._vars.centerX||0==this._vars.centerX){var g=this._vars.el,h=g.currentStyle||window.getComputedStyle(g);h.marginLeft.substring(0,h.marginLeft.length-2);this._vars.right||0==this._vars.right?a=f.left+f.width-this._vars.right-parseInt(b,10):this._vars.left||0==this._vars.left?a=f.left+this._vars.left:(this._vars.centerX||0==this._vars.centerX)&&(a=f.left+(f.width-parseInt(b,10))/2+this._vars.centerX),this._element.style.left=a+"px",this._self.originLeft=a}if(this._vars.top||0==this._vars.top||this._vars.bottom||0==this._vars.bottom||this._vars.centerY||0==this._vars.centerY){var g=this._vars.el,h=g.currentStyle||window.getComputedStyle(g);h.marginTop.substring(0,h.marginTop.length-2);this._vars.top||0==this._vars.top?a=f.top+this._vars.top:this._vars.bottom||0==this._vars.bottom?a=f.top+f.height-this._vars.bottom-parseInt(e,10):(this._vars.centerY||0==this._vars.centerY)&&(a=f.top+((f.height-e)/2+parseInt(this._vars.centerY))),this._element.style.top=a+"px",this._self.originTop=a}}}function f(){if("object"==typeof this._vars.push&&null!==this._vars.push){this._element.style.display="inline-block",this._element.style.clear="both",this._element.style.position="absolute";var a,b=this._element.getBoundingClientRect().width,c=this._element.offsetHeight,d=this._vars.push.el.getBoundingClientRect();(this._vars.push.bottom||0==this._vars.push.bottom)&&(a=d.top+d.height+this._vars.push.bottom,this._self.originTop=a,this._element.style.top=a+"px"),(this._vars.push.top||0==this._vars.push.top)&&(a=d.top-c-this._vars.push.top,this._element.style.top=a+"px",this._self.originTop=a),(this._vars.push.left||0==this._vars.push.left)&&(a=d.left-b-this._vars.push.left,this._element.style.left=a+"px",this._self.originLeft=a),(this._vars.push.right||0==this._vars.push.right)&&(a=d.left+d.width+this._vars.push.right,this._element.style.left=a+"px",this._self.originLeft=a)}}b.exports=function(a,b,c){return new d(a,b,c)}},{}],8:[function(a,b,c){var d,e=(a("./config.js"),a("./positioning.js")),f=a("./text.js"),g=a("./image.js"),h=a("./style.js"),i=a("./font.js"),j=0,k=[],l=[];b.exports=function(){var a={};a.font={},a.font.add=function(a,b){i.add(a,b)},a.position=function(a,b,c){var d=new e(a,b,c);k.push(d)},a.style=function(a,b){var c=new h(a,b);k.push(c)},a.text=function(a,b,c){var d=new f(a,b,c);k.push(d)},a.image=function(a,c,d){var e=new g(a,c,d,b);k.push(e),l.push(e)},a.loading={done:function(a){d=a,0==l.length&&d()}};var b=function(a,b){"error"==b&&(console.log("failed to load: "),console.log(a)),++j==l.length&&(d&&"function"==typeof d?d():console.log("no callback funciton given. Please add a function to the sr.loading.done function. IE: sr.loading.done(setPositioning)"))};return a.dynamicAssets=l,a.dd=k,a}()},{"./config.js":1,"./font.js":2,"./image.js":5,"./positioning.js":7,"./style.js":9,"./text.js":10}],9:[function(a,b,c){function d(a,b){this._element=a,this._vars=b||{},this._vars.greensock&&("undefined"!=typeof TweenMax?TweenMax.to(this._element,0,this._vars.greensock):console.log("no greensock, cannot add transform from style")),this._vars.css&&(cssProps=b.css,a.style.cssText+=";"+cssProps),this._vars.background&&(this._element.style["background-color"]=this._vars.background,this._element.style.setProperty("background-color",this._vars.background)),this._vars.width&&(this._element.style.width=this._vars.width+"px"),this._vars.height&&(this._element.style.height=this._vars.height+"px"),this._vars.addClass&&(this._element.className+=" "+this._vars.addClass)}b.exports=function(a,b){return new d(a,b)}},{}],10:[function(a,b,c){function d(a,b,c){if(this._element=a,this._vars=c,this._input=b||"",!this._element)return void console.log("kan element niet vinden");if(this._vars.css&&(this._element.style.cssText+="; "+this._vars.css),this.width=function(a){if(!a)return this._element.scrollWidth;this._element.style.width=a+"px"},this.height=function(){return this._element.offsetHeight},this._vars.addClass&&(this._element.className+=" "+this._vars.addClass),g.call(this),this._vars.autoSize){var d=document.createElement("div");d.id=this._element.id+"mirror";d.style.maxWidth=this._element.offsetWidth-0+"px",d.style.maxHeight=this._element.offsetHeight-0+"px",d.style.display="inline-block";var e=document.createElement("p");e.style.display="inline",e.id=this._element.id+"inner",d.style.lineHeight=this._element.style.lineHeight,d.style.letterSpacing=this._element.style.letterSpacing,d.style.fontSize=this._element.style.fontSize,d.style.fontWeight=this._element.style.fontWeight,d.style.fontFamily=this._element.style.fontFamily,e.innerHTML=this._element.innerHTML,d.appendChild(e),this._element.parentElement.appendChild(d);var f=document.getElementById(this._element.id+"inner");this._element.style.width=f.offsetWidth+1+"px",this._element.style.height=f.offsetHeight+1+"px",this._vars.debug||this._element.parentElement.removeChild(d)}}function e(){var a=this._vars.webfont.split(":");this._element.style.fontFamily=a[0],a[1]&&(this._element.style.fontWeight=a[1])}function f(){var a=window.getComputedStyle(this._element,null),b=a.getPropertyValue("font-size"),c=b.substring(0,b.length-2);return{fontSizeString:b||12,fontSize:c||12,fontFamily:this._element.style.fontFamily||"arial"}}var g=function(){(this._vars.height||this._vars.width)&&(this._element.style.width=this._vars.width+"px",this._element.style.height="auto"),this._vars.webfont&&e.call(this),this._vars.textAlign&&(this._element.style.textAlign=this._vars.textAlign),this._vars.color&&(this._element.style.color=this._vars.color),this._vars.background&&(this._element.style.backgroundColor=this._vars.background),h.call(this)},h=function(){this._vars.upperCase&&(this._valueOriginal=this._input,this._input=this._input.toUpperCase()),this._element.innerHTML=this._input,this._vars.fontSize?this._element.style.fontSize=this._vars.fontSize+"px":i.call(this)},i=function(){var a=this._vars.maxFs||60,b=this._vars.minFs||0;attempts=100*this._vars.maxFs||4e3,this._element.style.fontSize=a+"px";var c=f.call(this).fontSize||a;for(""==this._element.style.lineHeight&&(this._element.style.lineHeight=1,"SPAN"==this._element.tagName&&""==this._element.parentElement.style.lineHeight&&(this._element.parentElement.style.lineHeight=1));attempts&&(this.width()>this._element.offsetWidth||this._element.offsetWidth>this._vars.width||this.height()>this._vars.height);){if(parseInt(c)<parseInt(b))return this._element.style.width=this._vars.width+"px",this._element.style.height=this._vars.height+"px",this._element.style.overflow="hidden",this._element.style.textOverflow="ellipsis",void(this._element.className+=" tolong");var d=c-=.1;this._element.style.fontSize=d+"px",attempts--}};b.exports=d},{}],11:[function(a,b,c){function d(a,b){var c=a,d=f(a);d&&(c=d.id);var g=document.getElementById(c);return null==g&&(g=e(a,b)),g}function e(a,b){var c="div",d=f(a);d&&(c=d.tag,a=d.id);var e=b||document.getElementById("creative");if(!e)return void console.log("No div given, and creative div is not found");var g=document.createElement(c);return g.id=a,g.className="srDynamic",e.appendChild(g),g}function f(a){var b,c,d=a,e=/\[\S*\]/.exec(d);if(e){var f=e[0];b=f.substring(1,e[0].length-1),c=d.substring(f.length,d.length)}return!(!b||!c)&&{tag:b,id:c}}sr=a("./srmain.js"),group=a("./group.js");var g=function(){this.element=Object,this.elementArray=[],this.getArray=function(a){for(var b=a,c=[],d=0;d<b.length;d++)c.push(this.get(b[d]).element);return c},this.get=function(a){var b=a.replace(/\s+/g,"");if("string"==typeof b||b instanceof String){var c=b.split(">");if(1==c.length)b=c[0],g=d(b);else{var e=document.getElementById("creative");if(!e)return void console.log("no parent div given, and no creative div found");for(var f=0;f<c.length;f++){var g=d(c[f],e);e=g}}}else if(!(g=b))return void console.log("not element found and no string ");return this.element=g,this},this.height=function(a){if(this.elementArray.length)return group.groupHeight(this.elementArray)},this.width=function(a){return this.elementArray.length?group.groupWidth(this.elementArray):10},this.top=function(){var a=this.element.style.top;return a.substring(0,a.length-2)},this.left=function(){var a=this.element.style.left;return a.substring(0,a.length-2)},this.getPosition=function(a){if(this.elementArray.length)return group.groupGetPosition(this.elementArray)},this.loadPixel=function(a,b){var c=b||"default";if(void 0===srPixels[c]){var d=Math.random()+"",e=1e13*d;(new Image).src=a+e+"?",srPixels[c]=!0}},this.hotspot=function(a){var b=this.element.cloneNode(!0);return console.log(b),b.id=b.id+"_copy",b.style.zIndex=99999,b.style.opacity=0,b.style.cursor="pointer",a.rollover&&b.addEventListener("mouseover",function(){a.rollover()}),a.onclick&&b.addEventListener("click",function(){a.onclick()}),a.report,document.body.appendChild(b),this},this.position=function(a){if(this.elementArray.length&&a.group)group.position(this.elementArray,a);else if(this.elementArray.length)for(var b=0;b<this.elementArray.length;b++)sr.position(this.elementArray[b],a);else sr.position(this.element,a,this);return this},this.groupFs=function(){group.groupFs(this.elementArray)},this.text=function(a,b){if(null!==a&&"object"==typeof a&&(b=a,a=srElements[this.element.id].textValue||""),srElements[this.element.id]?(srElements[this.element.id].textStyle=Object.assign(srElements[this.element.id].textStyle,b),b=srElements[this.element.id].textStyle):(srElements[this.element.id]={},srElements[this.element.id].textStyle=b,srElements[this.element.id].textValue=a),this.elementArray.length)for(var c=0;c<this.elementArray.length;c++)sr.text(this.elementArray[c],a,b);else sr.text(this.element,a,b);return this},this.image=function(a,b){if(this.elementArray.length)for(var c=0;c<this.elementArray.length;c++)sr.image(this.elementArray[c],a,b);else sr.image(this.element,a,b);return this},this.style=function(a){if(this.elementArray.length)for(var b=0;b<this.elementArray.length;b++)sr.style(this.elementArray[b],a);else sr.style(this.element,a);return this},this.setSize=function(){console.log(this.nested()),console.log(this),this.element.style.width=this.nested().width+"px"||"0px",this.element.style.height=this.nested().height+"px"||"0px"},this.nested=function(a){function b(a){for(var d=0,e=a.children.length;d<e;d++){var h=a.children[d].getBoundingClientRect(),i=h.left+h.width,j=h.top+h.height;i>f&&(f=i,c=a.children[d]),j>g&&(g=j),b(a.children[d])}if(0==a.children.length){var h=a.getBoundingClientRect(),k=h.left+h.width;k>f&&(f=k,c=a.children[d]),j>g&&(g=j)}}var c,d=this.element,e=0,e=0,f=0,g=0;return e=d.getBoundingClientRect().left,relativeTop=d.getBoundingClientRect().top,b(d),{width:f-e,height:g-relativeTop}}};b.exports=g},{"./group.js":4,"./srmain.js":8}]},{},[3])(3)});
//# sourceMappingURL=sr.dynamic.min.js.map

/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

function loadSettings(callback) {

    var manifest = __webpack_require__(3);


    srBanner = {};
    srBanner.useDcs = manifest.data.dcs;
    srBanner.debug = manifest.debugging.debug;
    srBanner.playFrom = manifest.debugging.playFrom;
    srBanner.pauseFrom = manifest.debugging.pauseFrom;
    srBanner.backupImage = manifest.debugging.backupImage;

    //srbannerIslocal
    srBanner.isLocal = true;
    //endsrbannerIslocal
    srBanner.localFolder = "../set/assets/";

    srBanner.dynamicData = __webpack_require__(4);

    srBanner.feed = manifest.data.feed;

    if (srBanner.debug) {

        console.log("%c DEBUGGING SET TO TRUE", 'background: #FF0000; color: #FFFFFF');
    }

    asset = function(assetToLoad) {

        if (srBanner && srBanner.isLocal) {

            if (srBanner.debug) {
                console.log("%cload asset >>>>>> " + assetToLoad, 'background: #00c1ff; color: #FFFFFF');
            }
            return srBanner.localFolder + assetToLoad;

        }

        return "./" + assetToLoad;
    }

    dimension = function(assetToLoad, formatString) {


        console.log("asset to load from dimension");
        console.log(assetToLoad);

        console.log("srbanner is local: " + srBanner.isLocal)

        if (srBanner && srBanner.isLocal) {

            if (srBanner.debug) {
                console.log("%cload asset >>>>>> " + assetToLoad, 'background: #00c1ff; color: #FFFFFF');
            }
            return srBanner.localFolder + assetToLoad;

        }

  

        return "./" + assetToLoad;
    }


    return srBanner;

}


module.exports.loadSettings = loadSettings;

/***/ }),
/* 3 */
/***/ (function(module, exports) {

module.exports = {"publish":{"projectName":"sr_workflow_manifestname"},"debugging":{"debug":true,"playFrom":false,"pauseFrom":false,"backupImage":false},"data":{"feed":"zakelijk_duurzaam_see_poffertjes.js","dcs":false}}

/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

var setVersion = __webpack_require__(5);

function setup(callback) {

    if (srBanner.debug) {

        console.log("%c ==============feed=============", 'background: #0060a1; color: #FFFFFF');
        console.log("USING LOCAL FEED");
        console.log("%c _______________________________", 'background: #0060a1; color: #FFFFFF');

    }

    var devDynamicContent = {};

    var devDynamicContent = setVersion();

    var dd = devDynamicContent.srFeed[0];

    callback(dd);

}

module.exports.setup = setup;

/***/ }),
/* 5 */
/***/ (function(module, exports) {

function setData(callback) {

    var devDynamicContent = {};

    devDynamicContent.srFeed = [{}];
    devDynamicContent.srFeed[0]._id = 0;
    devDynamicContent.srFeed[0].isWorking = "Default feed 2";
    devDynamicContent.srFeed[0].exit_url = "";
    devDynamicContent.srFeed[0].versie = "think_poffertjes";
    devDynamicContent.srFeed[0].copy = {};
    devDynamicContent.srFeed[0].copy.h1 = "Kampioen poffertjes draaien?"; 
    devDynamicContent.srFeed[0].copy.h2 = "Maar geen verstand van energiewetgeving?";
    if (typeof(config) !== 'undefined') {
        if(config.bannerWidth == 120){
            devDynamicContent.srFeed[0].copy.h2 = "Maar geen verstand van energie-<br />wetgeving?";
        }
    }
    devDynamicContent.srFeed[0].copy.end_h1 = "Energie besparen wordt steeds makkelijker";
    devDynamicContent.srFeed[0].copy.end_txt = "Check in 1 minuut aan welke verplichtingen uw bedrijf moet voldoen";

    devDynamicContent.srFeed[0].cta = "Doe de check";
    devDynamicContent.srFeed[0].cta_sky = "Doe de check";
    
    devDynamicContent.srFeed[0].icon = asset("check_icon.png");
    
    devDynamicContent.srFeed[0].background336x280_1 = dimension("poffertjes-1-336x280.jpg", "336x280");
    devDynamicContent.srFeed[0].background336x280_2 = dimension("poffertjes-2-336x280.jpg", "336x280");

    devDynamicContent.srFeed[0].background300x250_1 = dimension("poffertjes-1-300x250.jpg", "300x250");
    devDynamicContent.srFeed[0].background300x250_2 = dimension("poffertjes-2-300x250.jpg", "300x250");

    devDynamicContent.srFeed[0].background320x240_1 = dimension("poffertjes-1-300x250.jpg", "320x240");
    devDynamicContent.srFeed[0].background320x240_2 = dimension("poffertjes-2-300x250.jpg", "320x240");

    devDynamicContent.srFeed[0].background728x90_1 = dimension("poffertjes-1-728x90.jpg", "728x90");
    devDynamicContent.srFeed[0].background728x90_2 = dimension("poffertjes-2-728x90.jpg", "728x90");

    devDynamicContent.srFeed[0].background970x250_1 = dimension("poffertjes-1-970x250.jpg", "970x250");
    devDynamicContent.srFeed[0].background970x250_2 = dimension("poffertjes-2-970x250.jpg", "970x250");

    devDynamicContent.srFeed[0].background120x600_1 = dimension("poffertjes-1-120x600.jpg", "120x600");
    devDynamicContent.srFeed[0].background120x600_2 = dimension("poffertjes-2-120x600.jpg", "120x600");

    devDynamicContent.srFeed[0].background160x600_1 = dimension("poffertjes-1-160x600.jpg", "160x600");
    devDynamicContent.srFeed[0].background160x600_2 = dimension("poffertjes-2-160x600.jpg", "160x600");

    devDynamicContent.srFeed[0].background300x600_1 = dimension("poffertjes-1-300x600.jpg", "300x600");
    devDynamicContent.srFeed[0].background300x600_2 = dimension("poffertjes-2-300x600.jpg", "300x600");
    return devDynamicContent;
}

module.exports = setData;

/***/ }),
/* 6 */
/***/ (function(module, exports) {

function setElements(callback) {

    config = {};
    config.bannerWidth = 300;
    config.bannerHeight = 600;

    addClicktag(dd.exit_url);
    loadFont();

    function loadFont() {
        sr.font.add([asset('bold.woff'),
            asset('semibold.woff')
        ], add);
    }

    function add() {
            
        ___("bg_1")
            .image(dd.background300x600_1, { width:300, height:600, fit:true });

        ___("bg_2")
            .image(dd.background300x600_2, { width:300, height:600, fit:true })
            .style({ greensock:{ alpha:0 } });

        ___("h1")
            .text(dd.copy.h1, { fontSize:26, color:"rgba(255,255,255,0)", webfont:"semibold" })
            .style({ background:"#D92266", css:"max-width:240px;" })
            .position({ left:20, top:260 });

        ___("h2")
            .text(dd.copy.h2, { fontSize:26, color:"rgba(255,255,255,0)", webfont:"semibold" })
            .style({ background:"#D92266", css:"max-width:240px;" })
            .position({ left:20, top:260 });

            if (dd.versie == "see_it" || dd.versie == "think_it" || dd.versie == "see_bouw" || dd.versie == "think_bouw") {
                ___("h2")
                    .position({ left:20, push:{ el: __("h1"), bottom:50 } })
            }

        ___("pink")
            .style({ background:"#e60167", width:config.bannerWidth, height:config.bannerHeight })

        ___("stripe_1")
            .style({ background:"rgba(0,0,0,0.21)", width:150, height:0, greensock:{ rotation:-75 } })
            .position({ top:425, left:-150 })

        ___("stripe_2")
            .style({ background:"rgba(0,0,0,0.21)", width:150, height:0, greensock:{ rotation:50 } })
            .position({ top:380, left:280 })

        ___("end_h1")
            .text(dd.copy.end_h1, { fontSize:26, color:"#fff", webfont:"bold" })
            .style({ css:"max-width:240px;" })
            .position({ left:20, top:44 })

        ___("end_txt")
            .text(dd.copy.end_txt, { fontSize:18, color:"#fff", webfont:"semibold" })
            .style({ css:"max-width:240px;" })
            .position({ left:20, push:{ el:__("end_h1"), bottom:6 } })

        if (dd.icon != "") {
            ___("icon")
                .image(asset("check_icon.png"), { width:120, height:110, fit:true })
                .position({ left:20, push: {el:__("end_txt"), bottom:20 }})
        }

        ___("footer")
            .style({ width:300, height:85, background:"#fff" })
            .position({ bottom:0, left:0 });

        ___("footer>cta")
            .text(dd.cta, { webfont:"bold", fontSize:16, color:"#fff", textAlign:"center" })
            .style({ background:"#02B5CE", css:"border-radius:3px; padding:9px 13px; border-bottom:2px solid #016E81;" })
            .position({ top:25, right:20 });

        ___("footer>logo")
            .image(asset("logo_pink.png"), { width:90, height:30, fit:true })
            .position({ left:20, top:25 });

        sr.loading.done(callback);
    }
}


function addClicktag(customUrl) {
    document.getElementById("bg-exit").addEventListener('click', bgExitHandler, false);

    function bgExitHandler(e) {
        if (srBanner.dcs) {
            Enabler.exit('Background Exit');
        } else {
            window.open(clickTag);
        }
    }
}

module.exports.setElements = setElements;

/***/ }),
/* 7 */
/***/ (function(module, exports) {

function animate(callback) {

    setAnimation();

    function setAnimation() {
        __("creative").style.opacity = 1;

        var tl01 = new TimelineMax();
        var tlRollover = new TimelineMax({});

        tl01.from(__("h1"), 1, { x:-100, transformOrigin:"center left", scaleX:0, ease:Back.easeOut });
        tl01.to(__("h1"), 1, { color:"rgba(255,255,255,1)" }, "firstTextIn");
        tl01.to(__("line"), 1, { alpha:1 }, "firstTextIn");

        if (dd.versie == "see_it" || dd.versie == "think_it" || dd.versie == "see_bouw" || dd.versie == "think_bouw") {
            tl01.to(this, 3.5, {});
        }else{
            tl01.to(this, 1.3, {});
        }


        tl01.to(__("h1"), 0.3, { color:"rgba(255,255,255,0)" }, "firstTextOut");
        tl01.to(__("line"), 0.3, { alpha:0 }, "firstTextOut");
        tl01.to(__("h1"), 0.6, { scaleX:0, transformOrigin:"center left" });

        if (dd.versie == "see_it" || dd.versie == "think_it" || dd.versie == "see_bouw" || dd.versie == "think_bouw") {
           tl01.from(__("h2"), 1, { x:100, transformOrigin:"center left", scaleX:0, ease:Back.easeOut }, "firstTextIn+=1");
            tl01.to(__("h2"), 1, { color:"rgba(255,255,255,1)" }, "firstTextIn+=1.5");
            tl01.to(__("h2"), 0.3, { color:"rgba(255,255,255,0)" }, "firstTextOut");
            tl01.to(__("h2"), 0.6, { scaleX:0, transformOrigin:"center left" }, "firstTextOut");
        }else{
            tl01.to(__("bg_2"), 1, { alpha:1 }, "second");
            tl01.from(__("h2"), 1, { x:100, transformOrigin:"center left", scaleX:0, ease:Back.easeOut }, "second");
            tl01.to(__("h2"), 1, { color:"rgba(255,255,255,1)" });
            tl01.to(this, 1.3, {});
            tl01.to(__("h2"), 0.3, { color:"rgba(255,255,255,0)" });
            tl01.to(__("h2"), 0.6, { scaleX:0, transformOrigin:"center left" });
            tl01.to(this, 0.2, {});
        }


        tl01.from(__("pink"), 0.5, { alpha:0 })
        tl01.to(__("stripe_1"), 1, { height:400, transformOrigin:"top center", ease:Back.easeOut })
        tl01.to(__("stripe_2"), 1, { height:400, transformOrigin:"top center", ease:Back.easeOut }, "-=0.5")
        tl01.from(__("end_h1"), 0.5, { alpha:0 }, "end_text-=0.5")
        tl01.from(__("end_txt"), 0.5, { alpha:0 }, "end_text-=0.5")
        
        tl01.fromTo(__("icon"), 1, { alpha:0, scale:0, rotation:-15, ease:Back.easeIn }, { alpha:1, scale:1.5, rotation:15  }, "end_text-=0.5")
        tl01.to(__("icon"), 0.75, { scale:1, rotation:0, ease:Back.easeOut  }, "end_text+=0.75")
        
        tl01.to(__("cta"), 0.5, { scale:1.1, yoyo:true, repeat:1, ease:Power2.easeInOut, transformOrigin:"center center" });
        tl01.to(this, 0, { onComplete:rollover });

        tlRollover.to(__("cta"), 0.5, { scale:1.1, yoyo:true, repeat:1, ease:Power2.easeInOut, transformOrigin:"center center" }, "rollover");

        function rollover(){
            __("banner").onmouseover = function(){
                tlRollover.play(0);
            };

            __("banner").onmouseout = function(){
                tlRollover.reverse();
            };
        }

        if (srBanner.debug) {
            if (srBanner.debug && srBanner.pauseFrom) {
                console.log("pause from " + srBanner.pauseFrom);
                tl01.pause(srBanner.pauseFrom);
            }

            if (srBanner.debug && srBanner.playFrom) {
                console.log("play from " + srBanner.playFrom);
                tl01.pause(srBanner.playFrom);
            }

            if (srBanner && srBanner.backupImage) {
                console.log("create backup images of last frame");
                tl01.add("screenshot");
                tl01.pause("screenshot");
            }
        }
    }

    if (callback) {
        callback();
    }
}

module.exports.animate = animate;

/***/ })
/******/ ]);