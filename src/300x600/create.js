function setElements(callback) {

    config = {};
    config.bannerWidth = 300;
    config.bannerHeight = 600;

    addClicktag(dd.exit_url);
    loadFont();

    function loadFont() {
        sr.font.add([asset('bold.woff'),
            asset('semibold.woff')
        ], add);
    }

    function add() {
            
        ___("bg_1")
            .image(dd.background300x600_1, { width:300, height:600, fit:true });

        ___("bg_2")
            .image(dd.background300x600_2, { width:300, height:600, fit:true })
            .style({ greensock:{ alpha:0 } });

        ___("h1")
            .text(dd.copy.h1, { fontSize:26, color:"rgba(255,255,255,0)", webfont:"semibold" })
            .style({ background:"#D92266", css:"max-width:240px;" })
            .position({ left:20, top:260 });

        ___("h2")
            .text(dd.copy.h2, { fontSize:26, color:"rgba(255,255,255,0)", webfont:"semibold" })
            .style({ background:"#D92266", css:"max-width:240px;" })
            .position({ left:20, top:260 });

            if (dd.versie == "see_it" || dd.versie == "think_it" || dd.versie == "see_bouw" || dd.versie == "think_bouw") {
                ___("h2")
                    .position({ left:20, push:{ el: __("h1"), bottom:50 } })
            }

        ___("pink")
            .style({ background:"#e60167", width:config.bannerWidth, height:config.bannerHeight })

        ___("stripe_1")
            .style({ background:"rgba(0,0,0,0.21)", width:150, height:0, greensock:{ rotation:-75 } })
            .position({ top:425, left:-150 })

        ___("stripe_2")
            .style({ background:"rgba(0,0,0,0.21)", width:150, height:0, greensock:{ rotation:50 } })
            .position({ top:380, left:280 })

        ___("end_h1")
            .text(dd.copy.end_h1, { fontSize:26, color:"#fff", webfont:"bold" })
            .style({ css:"max-width:240px;" })
            .position({ left:20, top:44 })

        ___("end_txt")
            .text(dd.copy.end_txt, { fontSize:18, color:"#fff", webfont:"semibold" })
            .style({ css:"max-width:240px;" })
            .position({ left:20, push:{ el:__("end_h1"), bottom:6 } })

        if (dd.icon != "") {
            ___("icon")
                .image(asset("check_icon.png"), { width:120, height:110, fit:true })
                .position({ left:20, push: {el:__("end_txt"), bottom:20 }})
        }

        ___("footer")
            .style({ width:300, height:85, background:"#fff" })
            .position({ bottom:0, left:0 });

        ___("footer>cta")
            .text(dd.cta, { webfont:"bold", fontSize:16, color:"#fff", textAlign:"center" })
            .style({ background:"#02B5CE", css:"border-radius:3px; padding:9px 13px; border-bottom:2px solid #016E81;" })
            .position({ top:25, right:20 });

        ___("footer>logo")
            .image(asset("logo_pink.png"), { width:90, height:30, fit:true })
            .position({ left:20, top:25 });

        sr.loading.done(callback);
    }
}


function addClicktag(customUrl) {
    document.getElementById("bg-exit").addEventListener('click', bgExitHandler, false);

    function bgExitHandler(e) {
        if (srBanner.dcs) {
            Enabler.exit('Background Exit');
        } else {
            window.open(clickTag);
        }
    }
}

module.exports.setElements = setElements;