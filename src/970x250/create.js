function setElements(callback) {

    config = {};
    config.bannerWidth = 970;
    config.bannerHeight = 250;

    addClicktag(dd.exit_url);
    loadFont();

    function loadFont() {
        sr.font.add([asset('bold.woff'),
            asset('semibold.woff')
        ], add);
    }

    function add() {
        ___("bg_1")
            .image(dd.background970x250_1, { width:970, height:250, fit:true });

        ___("bg_2")
            .image(dd.background970x250_2, { width:970, height:250, fit:true })
            .style({ greensock:{ alpha:0 } });

        ___("h1")
            .text(dd.copy.h1, { fontSize:26, color:"rgba(255,255,255,0)", webfont:"semibold" })
            .style({ background:"#D92266", css:"padding:11px; max-width:210px;" })
            .position({ left:414, centerY:0 });

        ___("h2")
            .text(dd.copy.h2, { fontSize:26, color:"rgba(255,255,255,0)", webfont:"semibold" })
            .style({ background:"#D92266", css:"padding:11px; max-width:200px;" })
            .position({ left:414, centerY:0 });

            if (dd.versie == "see_it" || dd.versie == "think_it" || dd.versie == "see_bouw" || dd.versie == "think_bouw") {
                ___("h2")
                    .position({ centerY:0, push:{ el: __("h1"), right:5 } })
            }

        ___("pink")
            .style({ background:"#e60167", width:config.bannerWidth, height:config.bannerHeight })

        ___("stripe_1")
            .style({ background:"rgba(0,0,0,0.21)", width:150, height:0, greensock:{ rotation:-45 } })
            .position({ top:20, left:-250 })

        ___("stripe_2")
            .style({ background:"rgba(0,0,0,0.21)", width:150, height:0, greensock:{ rotation:20 } })
            .position({ top:-40, left:-40 })

        ___("end_h1")
            .text(dd.copy.end_h1, { fontSize:34, color:"#fff", webfont:"bold" })
            .style({ css:"max-width:375px;" })
            .position({ left:310, top:47 })

        ___("end_txt")
            .text(dd.copy.end_txt, { fontSize:22, color:"#fff", webfont:"semibold" })
            .style({ css:"max-width:440px;" })
            .position({ left:310, push:{ el:__("end_h1"), bottom:6 } })

        if (dd.icon != "") {
            ___("icon")
                .image(asset("check_icon.png"), { width:140, height:150, fit:true })
                .position({ centerY:10, left:150 })
        }

        ___("footer")
            .style({ width:350, height:90 })
            .position({ bottom:0, right:0 });

        ___("cta")
            .text(dd.cta, { webfont:"bold", fontSize:17, color:"#fff", textAlign:"center" })
            .style({ background:"#02B5CE", css:"border-radius:3px; padding:12px 16px; border-bottom:2px solid #016E81;" })
            .position({ bottom:20, right:18 });

        ___("logo")
            .image(asset("logo_white.png"), { width:130, height:42, fit:true })
            .position({ right:20, top:20 });

        sr.loading.done(callback);
    }
}


function addClicktag(customUrl) {
    document.getElementById("bg-exit").addEventListener('click', bgExitHandler, false);

    function bgExitHandler(e) {
        if (srBanner.dcs) {
            Enabler.exit('Background Exit');
        } else {
            window.open(clickTag);
        }
    }
}

module.exports.setElements = setElements;