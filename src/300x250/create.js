function setElements(callback) {

    config = {};
    config.bannerWidth = 300;
    config.bannerHeight = 250;

    addClicktag(dd.exit_url);
    loadFont();

    function loadFont() {
        sr.font.add([asset('bold.woff'),
            asset('semibold.woff')
        ], add);
    }

    function add() {
            
        ___("bg_1")
            .image(dd.background300x250_1, { width:300, height:250, fit:true });

        ___("bg_2")
            .image(dd.background300x250_2, { width:300, height:250, fit:true })
            .style({ greensock:{ alpha:0 } });

        ___("h1")
            .text(dd.copy.h1, { fontSize:18, color:"rgba(255,255,255,0)", webfont:"semibold" })
            .style({ background:"#D92266", css:"padding:11px; max-width:200px;" })
            .position({ left:14, bottom:80 });

        ___("h2")
            .text(dd.copy.h2, { fontSize:18, color:"rgba(255,255,255,0)", webfont:"semibold" })
            .style({ background:"#D92266", css:"padding:11px; max-width:220px;" })
            .position({ left:14, bottom:80 });

        ___("pink")
            .style({ background:"#e60167", width:config.bannerWidth, height:config.bannerHeight })

        ___("stripe_1")
            .style({ background:"rgba(0,0,0,0.21)", width:150, height:0, greensock:{ rotation:-45 } })
            .position({ top:-90, left:100 })

        ___("stripe_2")
            .style({ background:"rgba(0,0,0,0.21)", width:150, height:0, greensock:{ rotation:20 } })
            .position({ top:0, right:-120 })

        ___("end_h1")
            .text(dd.copy.end_h1, { fontSize:22, color:"#fff", webfont:"bold" })
            .style({ css:"max-width:270px;" })
            .position({ left:12, top:44 })

        ___("end_txt")
            .text(dd.copy.end_txt, { fontSize:14, color:"#fff", webfont:"semibold" })
            .style({ css:"max-width:180px;" })
            .position({ left:12, push:{ el:__("end_h1"), bottom:6 } })

        if (dd.icon != "") {
            ___("icon")
                .image(asset("check_icon.png"), { width:81, height:75, fit:true })
                .position({ el:__("end_txt"), top:0 })
                .position({ right:10 })
        }

        ___("footer")
            .style({ width:336, height:60, background:"#fff" })
            .position({ bottom:0, left:0 });

        ___("footer>cta")
            .text(dd.cta, { webfont:"bold", fontSize:14, color:"#fff", textAlign:"center" })
            .style({ background:"#02B5CE", css:"border-radius:3px; padding:7px 11px; border-bottom:2px solid #016E81;" })
            .position({ top:12, left:14 });

        ___("footer>logo")
            .image(asset("logo_pink.png"), { width:90, height:30, fit:true })
            .position({ right:14, top:14 });

        sr.loading.done(callback);
    }
}


function addClicktag(customUrl) {
    document.getElementById("bg-exit").addEventListener('click', bgExitHandler, false);

    function bgExitHandler(e) {
        if (srBanner.dcs) {
            Enabler.exit('Background Exit');
        } else {
            window.open(clickTag);
        }
    }
}

module.exports.setElements = setElements;