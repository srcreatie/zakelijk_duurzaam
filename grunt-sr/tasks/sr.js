/*
 * grunt-sr
 * www.searchresuult.nl
 *
 * Copyright (c) 2016 Tim Wijnhoven
 * Licensed under the Closed license.
 */

'use strict';

module.exports = function(grunt) {

    // Please see the Grunt documentation for more information regarding task
    // creation: http://gruntjs.com/creating-tasks

    grunt.registerMultiTask('sr', 'Pick files to be included in build', function() {
        // Merge task-specific and/or target-specific options with these defaults.
        var options = this.options({
            punctuation: '.',
            separator: ', '
        });

        console.log("The following files are found");


        var assets = getAssetsArray();

        var assetPerDimension = getAssetsArrayTest();

        console.log("per folder");
        console.log(options.currentFolder);
        console.log(assetPerDimension);


        console.log("assets");
        // console.log(assets);

        // console.log(assets);

        var combinedAssetArry = assets.concat(assetPerDimension);

        console.log(combinedAssetArry);



        var linksToFiles = getFilenameLinks(combinedAssetArry);

        // var linksToFilesPerDimension = getFilenameLinks(assetPerDimension);

        var externalfileList = {};
        externalfileList.files = linksToFiles

        // grunt.task.run('copy');

        grunt.config("assetfiles", combinedAssetArry)

        // console.log(JSON.stringify(externalfileList))

        // console.log(externalfileList);

        // console.log(options.currentFolder);


        grunt.file.write(options.directory + "/minifest-" + options.currentFolder + "-filelist.json", JSON.stringify(externalfileList))
        return linksToFiles;


        function getAssetsArrayTest() {

            var content;

            for (var i = 0; i < options.files[0].src.length; i++) {


                content += grunt.file.read(options.files[0].src[i]);

            }

            var foundAssets = [];

            var regex = new RegExp(/(dimension.*)/g);

            var assetFile;

            while ((assetFile = regex.exec(content)) != null) {
                // console.log(assetFile[1]);



                var assetFileName = assetFile[1].split("(\"")[1];
                // console.log("assetfile.");
                // console.log(assetFileName);

                var assetTest = assetFileName.split(",");
                // console.log("filename.");
                var fileNameOfFile = assetTest[0].substring(0, assetTest[0].length - 1);

                // console.log("format.");
                // console.log(assetTest[1].substring(0, assetTest[1].length - 2));

                var testing = assetTest[1].substring(2, assetTest[1].length - 3);

                // var testing2 = testing.substring(1, testing.length - 1)

                // console.log("filename and folder");

                // console.log(options.currentFolder);

                // console.log(testing);

                if (testing == options.currentFolder) {

                    // console.log("should be in the manifest");
                    foundAssets.push(fileNameOfFile);
                }
                // if (!assetFileName) {

                //     assetFileName = assetFile[1].split("(\'")[1];
                // }

                // ooksplittenopenkeleaanhalingsteken;


            }
            return foundAssets;
        }



        function getAssetsArray() {

            var content;

            for (var i = 0; i < options.files[0].src.length; i++) {


                content += grunt.file.read(options.files[0].src[i]);

            }

            var foundAssets = [];

            var regex = new RegExp(/(asset.*\.(png|jpg|woff))/g);


            var assetFile;

            while ((assetFile = regex.exec(content)) != null) {
                // console.log(assetFile[1]);

                var assetFileName = assetFile[1].split("(\"")[1];

                if (!assetFileName) {

                    assetFileName = assetFile[1].split("(\'")[1];
                }

                // ooksplittenopenkeleaanhalingsteken;

                foundAssets.push(assetFileName);

            }
            return foundAssets;
        }

        function getFilenameLinks(assetArray) {

            var assetLinksFound = [];

            for (var i = 0; i < assetArray.length; i++) {

                var filename = "**/" + assetArray[i];
                console.log(filename);
                assetLinksFound.push(filename);
            }
            return assetLinksFound;


        }

        // Iterate over all specified file groups.
        this.files.forEach(function(f) {
            // Concat specified files.
            var src = f.src.filter(function(filepath) {
                // Warn on and remove invalid source files (if nonull was set).
                if (!grunt.file.exists(filepath)) {
                    grunt.log.warn('Source file "' + filepath + '" not found.');
                    return false;
                } else {
                    return true;
                }
            }).map(function(filepath) {
                // Read file source.
                return grunt.file.read(filepath);
            }).join(grunt.util.normalizelf(options.separator));

            // Handle options.
            src += options.punctuation;

            // Write the destination file.
            grunt.file.write(f.dest, src);

            // Print a success message.
            grunt.log.writeln('File "' + f.dest + '" created.');
        });
    });

};